const ONE = 1;
const FIRST = 0;

export declare type Query = any;
export declare type Projection = any;

export function optimizeAnd($and: Query[]): Query {
  if (!$and.length) return {};
  if ($and.length === ONE) {
    return $and[FIRST];
  }

  return {$and};
}
