/**
 * Sort result directions
 */
export enum Sort {
  ASCENDING = 1,
  DESCENDING = -1,
}
