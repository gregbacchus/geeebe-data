import { Document, Model, Mongoose, Schema } from 'mongoose';

const softDelete = require('mongoose-delete');
const snakeCase = require('snake-case');

export type Extension = (schema: Schema) => void;

export class Mongo {
  public static createSchema(schema: Schema, strict: boolean, extension?: Extension) {
    schema.set('timestamps', {
      createdAt: 'whenCreated',
      updatedAt: 'whenUpdated',
    });
    schema.set('strict', strict);
    schema.plugin(softDelete, { overrideMethods: true });
    extension && extension(schema);

    return schema;
  }

  constructor(
    private readonly mongoose: Mongoose,
    connectionUrl: string,
  ) {
    mongoose.Promise = global.Promise;
    mongoose.connect(connectionUrl);
  }

  public createModel<TEntity extends Document>(name: string, schema: Schema, strict: boolean, extension?: Extension): Model<TEntity> {
    return this.mongoose.model<TEntity>(name, Mongo.createSchema(schema, strict, extension), snakeCase(name));
  }
}
